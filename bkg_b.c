/*

 BKG_b.C

 Tile Source File.

 Info:
  Form                 : All tiles as one unit.
  Format               : Gameboy 4 color.
  Compression          : None.
  Counter              : None.
  Tile size            : 8 x 8
  Tiles                : 2 to 3

  Palette colors       : Included.
  SGB Palette          : None.
  CGB Palette          : None.

  Convert to metatiles : No.

 This file was generated by GBTD v1.8

*/

/* Start of tile array. */
unsigned char bkg_b[] =
{
  0x00,0x00,0xFE,0xFE,0xFE,0xAA,0xFE,0xAA,
  0xFE,0xAA,0xFE,0xAA,0xFE,0xAA,0xFE,0xAA,
  0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
  0x00,0x00,0x00,0x00,0x00,0x00,0xFE,0xFE
};

/* End of BKG_b.C */
