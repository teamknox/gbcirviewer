;*************************************************
;*                                               *
;*         IR DRIVER for Color Game Boy          *
;*                [ Ir-Receiver ]                *
;*                                               *
;*                   2000/01                     *
;*            TeamKNOx Kiyoshi Izumi             *
;*                                               *
;*************************************************
;Speed:  d  : clock
;------------------------------------------------
;   0 :  16 : 4.194304MHz *   0.1ms =  419.4clock
;   1 :	 17 : 4.194304MHz / 9600bps =  436.9clock
;   2 :  42 : 4.194304MHz *   0.2ms =  838.9clock
;   3 :  44 : 4.194304MHz / 4800bps =  873.8clock
;   4 :  95 : 4.194304MHz *   0.4ms = 1677.7clock
;   5 :  99 : 4.194304MHz / 2400bps = 1747.6clock
;   6 : 199 : 4.194304MHz *   0.8ms = 3355.4clock
;   7 : 208 : 4.194304MHz / 1200bps = 3495.3clock

	.area	_BSS
_rcv_buff::
	.ds	20*127		;2540bytes

	.area	_CODE
_ir_rcv::
	di
	lda	hl,2(sp)	;Skip return address
	ld	a,(hl)		;Speed
	cp	#<0
	jr	z,sp01
	cp	#<1
	jr	z,sp96
	cp	#<2
	jr	z,sp02
	cp	#<3
	jr	z,sp48
	cp	#<4
	jr	z,sp04
	cp	#<5
	jr	z,sp24
	cp	#<6
	jr	z,sp08
	jr	sp12
sp01:
	ld	d,#<16
	jr	rcv_init
sp96:
	ld	d,#<17
	jr	rcv_init
sp02:
	ld	d,#<42
	jr	rcv_init
sp48:
	ld	d,#<44
	jr	rcv_init
sp04:
	ld	d,#<95
	jr	rcv_init
sp24:
	ld	d,#<99
	jr	rcv_init
sp08:
	ld	d,#<199
	jr	rcv_init
sp12:
	ld	d,#<208
rcv_init:
	ldh	a,(#0x56)
	or	#0b11000000
	ldh	(#0x56),a	;Read Enable

	ld	hl,#_rcv_buff
	ld	e,#<127
rcv_chk:
	ldh	a,(#0x56)
	and	#0x02
	jr	nz,rcv_chk
rcv_next:
	ld	c,#<20
rcv_data:
	ldh	a,(#0x56)			;12
	ld	b,a				; 4
	ldh	a,(#0x56)			;12
	and	b				; 4
	ld	b,a				; 4
	ldh	a,(#0x56)			;12
	and	b				; 4
	ld	b,a				; 4
	ldh	a,(#0x56)			;12
	and	b				; 4
	ld	b,a				; 4
	ldh	a,(#0x56)			;12
	and	b				; 4
	rrca					; 4
	and	#0x01				; 8
	or	#0x02				; 8
	ld	(hl+),a				; 8
	ld	b,d				; 4
	call	wait				;12+16*d+12
	dec	c				; 4
	jr	nz,rcv_data			;12
	dec	e
	jr	nz,rcv_next
	
				;-----------------------
				;total clock   16*d+164
	ei
	ret
	
;=================================================
; wait
;	input		:b
;	output		:non
;	destroyed	:b
;=================================================
wait:
	dec	b				; 4
	jr	nz,wait				;12	-4
	ret					;	16
				;--------------------------
				;total clock 	 16	12


