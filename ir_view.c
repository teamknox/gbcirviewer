/*******************/
/*    Ir-Viewer    */
/*     2000/03     */
/*    TeamKNOx     */
/*******************/

#include <gb.h>
#include <stdlib.h>
#include "bkg.h"

/* definition */
#define	OFF		0
#define	ON		1

/* work area */
UBYTE key_flag;
UBYTE rvs;

/* extern */
extern unsigned char rcv_buff[127][20];

/* character data */
#include "bkg.c"
#include "bkg_a.c"
#include "bkg_b.c"
unsigned char *chara[]  = { bkg_a, bkg_b };
UWORD bkg_palette[] = {
  bkgCGBPal0c0, bkgCGBPal0c1, bkgCGBPal0c2, bkgCGBPal0c3,
  bkgCGBPal1c0, bkgCGBPal1c1, bkgCGBPal1c2, bkgCGBPal1c3,
  bkgCGBPal2c0, bkgCGBPal2c1, bkgCGBPal2c2, bkgCGBPal2c3
};

/* message */
unsigned char msg_title[] = { "[Ir-Viewer]" };
unsigned char msg_speed[][8] = { "0.1ms","9600bps","0.2ms","4800bps","0.4ms","2400bps","0.8ms","1200bps" };
unsigned char msg_ready[] = { "READY!" };
unsigned char msg_clear[] = { "                    " };
unsigned char msg_ruler[] = { "12345678901234567890" };

/* functoins */
void init_character()
{
  set_bkg_data( 0, 128, bkg );
  set_bkg_data( 2, 2, chara[rvs] );
  set_bkg_palette( 0, 3, bkg_palette );
  SHOW_BKG;
  DISPLAY_ON;
  enable_interrupts();
}

void cls()
{
  UBYTE y;
  for( y=0; y<18; y++ ) {
    set_bkg_tiles( 0, y, 20, 1, msg_clear );
  }
}

UBYTE getkey()
{
  UBYTE inkey;
  inkey = joypad();
  if( key_flag == OFF ) {
    if( inkey & (J_UP|J_DOWN|J_A) ) {
      key_flag = ON;
    }
  } else {
    if( !(inkey & (J_UP|J_DOWN|J_A)) ) {
      key_flag = OFF;
    }
    inkey = 0;
  }
  return( inkey );
}

void main()
{
  UBYTE key, row, i;
  UBYTE speed = 5;
  init_character();
  rvs = 0;
  key_flag = OFF;
  while( 1 ) {
    cls();
    set_bkg_tiles( 5, 4, 11, 1, msg_title );
    key = 0;
    while(key != J_A) {
      key = getkey();
      if ( (key & J_UP)&&(speed > 0) ) speed --;
      if ( (key & J_DOWN)&&(speed < 7) ) speed ++;
      set_bkg_tiles( 7, 8, 7, 1, msg_speed[speed] );
    }
    set_bkg_tiles( 7, 10, 6, 1, msg_ready );
    ir_rcv(speed);
    row = 0;
    set_bkg_tiles( 0, 0, 20, 1, msg_ruler );
    for (i=0; i<17; i++) {
      set_bkg_tiles( 0, i+1, 20, 1, rcv_buff[row+i] );
    }
    key = 0;
    while(key != J_START) {
      key = getkey();
      if ( (key & J_UP)&&(row > 0) ) row--;
      if ( (key & J_DOWN)&&(row < 127-17) ) row++;
      if ( key & J_A ) {
          rvs ^= 0x01;
          set_bkg_data( 2, 2, chara[rvs] );
      }
      if ( key ) {
        for (i=0; i<17; i++) {
          set_bkg_tiles( 0, i+1, 20, 1, rcv_buff[row+i] );
        }
      }
    }
  }
}

/* EOF */
